#!/bin/bash

DEVICE=alsa_input.usb-Sennheiser_Communications_Sennheiser_USB_headset-00.analog-mono

OUTPUT_FOLDER=/var/cache/sampler

BITS=16
ENCODING=signed
CHANNELS=1
RATE=44100

SAMPLE_DURATION=5
OUTFORMAT=wav

function foldername { date +%Y%m%d; }
function filename { date +%Y%m%d-%H%M%S; }

mkdir -p {$OUTPUT_FOLDER}

pushd ${OUTPUT_FOLDER} >/dev/null

DFOLDER=$(foldername)

mkdir -p $DFOLDER
cd $DFOLDER

parec --device=${DEVICE} --fix-format --fix-rate --channels=${CHANNELS} | \
    sox -t raw -b ${BITS} -e ${ENCODING} -c ${CHANNELS} -r ${RATE} - $(filename).${OUTFORMAT} trim 0 ${SAMPLE_DURATION}

popd >/dev/null