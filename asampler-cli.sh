#!/bin/bash

DEVICE=hw:1,0

OUTPUT_FOLDER=/var/cache/sampler

FORMAT=S16_LE
CHANNELS=1
RATE=44100

SAMPLE_DURATION=5
OUTFORMAT=wav

function foldername { date +%Y%m%d; }
function filename { date +%Y%m%d-%H%M%S; }

mkdir -p {$OUTPUT_FOLDER}

pushd ${OUTPUT_FOLDER} >/dev/null

DFOLDER=$(foldername)

mkdir -p $DFOLDER
cd $DFOLDER

arecord --device=${DEVICE} --format=${FORMAT} --channels=${CHANNELS} --rate=${RATE} --duration=${SAMPLE_DURATION} $(filename).${OUTFORMAT}

popd >/dev/null