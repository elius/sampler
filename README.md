# Main

Simple script for periodic recording audio samples.

PulseAudio version require pulseaudio server, parec and sox utility.

* pulseaudio-utils
* sox

ALSA version require alsa and alsa-utils utility.

* alsa
* alsa-utils
